/** Load into the data segments */
#[link_section = ".inits.005"]
#[no_mangle]
static __LOAD_DATA_SEGMENTS: fn() -> () = data_segments;

/** Loads the .data segment data from flash to memory if required.  */
fn data_segments() -> () {
    extern "C" {
        static mut __data_load: u32;
        static mut __data_store_start: u32;
        static mut __data_store_end: u32;
        static mut __bss_start: u32;
        static mut __bss_end: u32;
    }

    unsafe {
        let mut data_load_addr: *mut u32 = &mut __data_load;
        let mut store_cursor: *mut u32 = &mut __data_store_start;
        let data_store_end_addr: *mut u32 = &mut __data_store_end;

        while store_cursor < data_store_end_addr {
            store_cursor.write_volatile(*data_load_addr);
            data_load_addr = data_load_addr.offset(1);
            store_cursor = store_cursor.offset(1);
        }

        let bss_end: *mut u32 = &mut __bss_end;
        let mut bss_cursor: *mut u32 = &mut __bss_start;

        while bss_cursor < bss_end {
            bss_cursor.write_volatile(0);
            bss_cursor = bss_cursor.offset(1);
        }
    }

    return;
}
