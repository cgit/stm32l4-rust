#[link_section = ".on_reset"]
#[no_mangle]
pub static __RESET_VECTOR: fn() -> ! = on_reset;

/**
 * The init sections are run. The order in which they are run is deterined by the name of the
 * section the pointer to the function is located in. By convention the section name is 'inits.nnn'
 * where 'nnn' is a number from 000-999.
 */
fn run_init_sections() -> () {
    extern "C" {
        static mut _sinits: extern "C" fn() -> ();
        static mut _einits: extern "C" fn() -> ();
    }
    unsafe {
        let mut cursor: *mut extern "C" fn() -> () = &mut _sinits;
        let einits_end: *mut extern "C" fn() -> () = &mut _einits;

        while cursor < einits_end {
            cursor.read()();
            cursor = cursor.offset(1);
        }
    }
}

fn on_reset() -> ! {
    run_init_sections();
    loop {}
}
