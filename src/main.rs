#![no_main]
#![no_std]

use core::panic::PanicInfo;
use hal::gpio::get_gpio_port_b;

use crate::hal::gpio;

mod hal;
mod init;

#[link_section = ".inits.999"]
#[no_mangle]
pub static __MAIN: unsafe fn() -> () = main;
pub unsafe fn main() -> () {
    let mut pin = get_gpio_port_b().pin(3);
    pin.set_mode(gpio::GpioMode::Output);
    pin.set_output_type(gpio::GpioOutputType::PushPull);
    pin.set_output_speed(gpio::GpioSpeed::Medium);
    pin.set_pull_dir(gpio::GpioPullDir::Down);

    pin.set_high();
    loop {
        delay();
        pin.set_low();
        delay();
        pin.set_high();
    }
}

fn delay() {
    let mut cnt = 0;
    while cnt < 10000 {
        cnt += 1;
    }
}

#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    loop {}
}
