/*
 * Sets bits in the register pointed to by 'reg'.
 *
 * reg: raw pointer to the register to manipulate.
 * mask: the bits to change in the register. The binary of mask should follow
 *       the regex 0*1*0* (i.e. all the 1's should be contiguous).
 * val: The value to write to the bits referenced by 'mask'
 */
pub fn regset(reg: *mut u32, mask: u32, val: u32) -> () {
    unsafe {
        reg.write_volatile((reg.read_volatile() & !mask) | (val << mask.trailing_zeros()));
    }
}

/*
 * Returns bits in the register pointed to by 'reg'.
 *
 * reg: raw pointer to the register to read
 * mask: the bits to retrieve.
 */
pub fn regget(reg: *mut u32, mask: u32) -> u32 {
    unsafe {
        (reg.read_volatile() & mask) >> mask.trailing_zeros()
    }
}
