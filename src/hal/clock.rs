struct Pwr {
  cr  : u32,
  csr : u32,
}

extern "C" {
    static mut __Pwr_base : u32;
}

static PWR: *mut Pwr = __Pwr_base as *mut Pwr;
