#[repr(C)]
struct Usart {
    c_r1  : u32,
    c_r2  : u32,
    c_r3  : u32,

    br_r  : u32,
    gtp_r : u32,
    rto_r : u32,
    rq_r  : u32,

    is_r  : u32,
    ic_r  : u32,
    rd_r  : u32,
    td_r  : u32,
}
