
all:
	cargo build

main.bin: all
	arm-unknown-eabi-objcopy -O binary target/thumbv7em-none-eabihf/debug/stm32l4-rust main.bin

flash: main.bin
	openocd -f openocd.cfg -c "program main.bin reset exit 0x08000000"

target/thumbv7em-none-eabihf/debug/stm32l4-rust:
	cargo build

clean:
	cargo clean

debug:
	arm-unknown-eabi-gdb -tui -ex 'tar ext :3333' -ex 'file target/thumbv7em-none-eabihf/debug/stm32l4-rust'
